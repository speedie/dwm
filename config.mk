# dwm compilation options
#
# Depending on your configuration, you may need to uncomment some lines here.

# OpenBSD support
# If you use OpenBSD, uncomment this line. (remove the # at the start of the line below)
#FREETYPEINC = ${X11INC}/freetype2

# Solaris support
# If you use Solaris, uncomment this line. (remove the # at the start of the line below)
#CFLAGS = -fast ${INCS} -DVERSION=\"${VERSION}\"
#LDFLAGS = ${LIBS}

# Xinerama, comment these lines if you don't want multimonitor support
# If you don't know what this means, don't change anything and install:
# Arch: libxinerama
# Gentoo: libXinerama
# Debian: libXinerama-devel
XINERAMALIBS  = -lXinerama
XINERAMAFLAGS = -DXINERAMA

# Compiling with -Ofast. If you're having issues (such as dwm crashing, change it to -O2. (That's not a zero)
# Using -O2 is not recommended due to the usage of Imlib2.
# If these features are used, they will be slow with the use of -O2.
CFLAGS   = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Ofast ${INCS} ${CPPFLAGS}
LDFLAGS  = ${LIBS}

# Compiler and linker.
# You can swap this out if you know what you're doing.
CC = cc

# Paths
# These should be fine for most users but if you use a distribution of GNU/Linux like NixOS or GNU Guix, consider changing this to fit your use case.
# HTMLDIR is only useful if you are using 'make release'.
PREFIX = /usr
HTMLDIR = "/home/anon/Projects/spdgmr.github.io/"

###################################################################################

# dwm version
VERSION = 1.1-11082022

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

# freetype
FREETYPELIBS = -lfontconfig -lXft
FREETYPEINC = /usr/include/freetype2

# includes and libs
INCS = -I${X11INC} -I${FREETYPEINC}
LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS} ${FREETYPELIBS} -lXrender -lImlib2 -lX11-xcb -lxcb -lxcb-res -lXext

# flags
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\" ${XINERAMAFLAGS}
