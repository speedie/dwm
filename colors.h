/* Color/Alpha settings
 * You probably don't need to change these unless you want special opacity settings.
 *
 * Once you're done with your edits, run 'make clean install'. */

/* Misc color options */
static char *colors[][3]                      = {
	[SchemeBar]                               = { col_textnorm,         col_background,       col_windowbordernorm },
	[SchemeTags]                              = { col_textnorm,         col_background,       col_windowbordernorm },
	[SchemeNormBorder]                        = { col_textnorm,         col_background,       col_windowbordernorm },
	[SchemeSelBorder]                         = { col_textsel,          col_windowbordernorm, col_windowbordersel }, 
	[SchemeStatus]                            = { col_textnorm,			col_textsel,          col_textsel },
	[SchemeNormTitle]                         = { col_textnorm,         col_background,       col_background },
	[SchemeSelTitle]                          = { col_textsel,          col_backgroundmid,    col_textsel },
	[SchemeHid]                               = { col_backgroundmid,    col_background,       col_backgroundmid  },
	[SchemeLayout]                            = { col_layouttext,       col_layoutbgsel,      col_layoutbgnorm },
/*                                                text                  background            window border
*/
};

/* Colors for the status bar (.Xresources) */
static char *colstatus[]                      = {
  col_status0,
  col_status1,
  col_status2,
  col_status3,
  col_status4,
  col_status5,
  col_status6,
  col_status7,
  col_status8,
  col_status9,
  col_status10,
  col_status11,
  col_status12,
  col_status13,
  col_status14,
  col_status15,
};

/* Colors to use for opacity
*/

static const unsigned int alphas[][3]         = {
       /*                                         fg             bg                border     */
       [SchemeBar]                            = { OPAQUE,        baropacity,       baropacity },
	   [SchemeNormTitle]                      = { OPAQUE,        normtitleopacity, normtitleopacity },
	   [SchemeSelTitle]                       = { OPAQUE,        seltitleopacity,  seltitleopacity },
       [SchemeLayout]                         = { OPAQUE,        layoutopacity,    layoutopacity },
       [SchemeStatus]                         = { OPAQUE,        statusopacity,    statusopacity },
       [SchemeHid]                            = { hiddenopacity, hiddenopacity,    hiddenopacity },
	   [SchemeTags]                           = { tagselopacity, tagselopacity,    tagnormopacity },
};

/* Colors to use for tags */
static char *tagsel[][2]                      = {
	{ col_tag1_text, col_tag1 }, 
	{ col_tag2_text, col_tag2 },
	{ col_tag3_text, col_tag3 },
	{ col_tag4_text, col_tag4 },
	{ col_tag5_text, col_tag5 },
	{ col_tag6_text, col_tag6 },
	{ col_tag7_text, col_tag7 },
	{ col_tag8_text, col_tag8 },
	{ col_tag9_text, col_tag9 },
	/* Text       Background */
};

/* Alpha for tags */
static const unsigned int tagalpha[]          = {
		tagselopacity, 
		tagnormopacity,
};
