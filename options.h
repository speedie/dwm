/* speedie.gq's build of suckless.org's dynamic window manager (dwm).
 *
 *  This is a fork of suckless's dwm, a window manager for X.
 *  The header you are currently reading is for changing what software you want to use.
 *
 *  Because this build of dwm auto-starts software such as your compositor, you may want to change-
 *  this by changing '#define COMPOSITOR'. Same goes for other software.
 *  
 *  If you aren't aware of this, do not add anything to your .xinitrc if it is listed in autostart.h
 *
 *  - If you were looking for the keybinds, they can be found in keybinds.h
 *  - If you use another operating system such as OpenBSD, FreeBSD or Solaris (Not GNU/Linux), edit config.mk
 *
 * This build comes with defines for common software such as your web
 * browser or terminal.
 * If you wish to use a different browser for example, simply change the values.
 *
 * You do not need to edit 'keybinds.h' or 'rules.h' because that's where these are used.
 * Once you're done with your edits, run 'make clean install'.
 * 
 * Alternatively, you can change some of these in ~/.config/dwm-de/dwmrc.
 *************************************/

/* Software options
 * Note that any software named "dwm-*" is bundled with this build and does not need to be replaced unless you want to.
 */
#define TERMINAL                              "st -e "                                                                       /* Terminal to use */
#define BROWSER                               "firefox"																		 /* Web browser to use */
#define SYSTEMSTAT                            "htop"                                                                         /* System stat viewer to use */
#define RSS                                   "newsboat"                                                                     /* RSS reader to use */
#define PDF                                   "zathura"                                                                      /* PDF reader to use */
#define MUSIC                                 "cmus"                                                                         /* Music player to use */
#define VISUALIZER                            "~/.local/bin/vis || vis"                                                      /* Music visualizer to use */
#define EMAIL                                 "aerc"																		 /* Email client to use */
#define EDITOR                                "nvim"                                                                          /* Text editor to use */
#define RUN                                   "dmenu_run -l 0 -p 'Run:' -h 19"                                               /* Run launcher */
#define SCREENSHOT                            "dwm-screenshotutil -s"														 /* How to take screenshots (Selection) */
#define SCREENSHOT_FULL                       "dwm-screenshotutil -f"														 /* How to take screenshots (Full screen) */
#define FILEMANAGER                           "vifmrun || vifm"                                                              /* File manager that will be used */
#define LOCKER                                "slock"                                                                        /* Screen locker that will be used */
#define OPENPDF                               "dwm-pdfopen ~/Documents zathura || ~/Scripts/dwm-pdfopen ~/Documents zathura" /* Command to run when listing and opening PDFs. */
#define KILLMUSIC                             "pkill cmus"                                                                   /* Command to run when killing the music player */

/* Software classes */
#define TERMINAL_CLASS                        "St"                                                                           /* Terminal to use for rules */
#define BROWSER_CLASS                         "firefox"																		 /* Web browser to use for rules */
#define PDF_CLASS                             "Zathura"                                                                      /* PDF reader to use for rules */
#define FILEMANAGER_CLASS                     "vifmrun"                                                                      /* File manager that will be used for rules */

/* Audio options */
#define MIXER                                 "dwm-audioctrl -runmixer"                                                      /* Audio mixer to use */
#define VOL_DOWN                              "dwm-audioctrl -lower"                                                         /* Command to run when decreasing volume */
#define VOL_UP                                "dwm-audioctrl -raise"                                                         /* Command to run when increasing volume */
#define VOL_MUTE                              "dwm-audioctrl -mute"                                                          /* Command to run when muting volume */
#define VOL_OUTPUT_SPEAKER                    "dwm-audioctrl -switch"                                                        /* Command to run when enabling speakers */

/* Modifier keys
 * Mod4Mask | Super (Windows/command) key
 * Mod1Mask | Alt key
 */
#define MODKEY                                Mod4Mask
#define SMODKEY                               Mod1Mask

/* Managers */
#define CLIPBOARD                             "xclip"                                                                        /* Clipboard to use */
#define COMPOSITOR                            "picom --experimental-backends"												 /* Compositor to use */
#define NETWORK                               "dwm-netctrl"                                                                  /* Network manager to use */
#define BLUETOOTH                             "dwm-btctrl"                                                                   /* Bluetooth manager to use */
#define NOTIFICATION                          "dunst"                                                                        /* Notification daemon to use */

/* Paths */
#define SESSION_FILE                          "/tmp/dwm-session"

/* Misc */
#define SHCMD(cmd)                            { .v = (const char*[]){ shell, "-c", cmd, NULL } }
#define TAGKEYS(CHAIN,KEY,TAG)                { MODKEY, CHAIN, KEY, view, {.ui = 1 << TAG} },	\
										      { MODKEY|ShiftMask, CHAIN, KEY, previewtag, {.ui = TAG} },
/* Options
 *
 * If xrdb is installed, you can simply edit ~/.config/dwm-de/dwmrc instead of recompiling.
 * Once you're done with your edits, run 'make clean install'.
 */

/* Window alignment options */
static unsigned int borderpx                  = 1; /* How big your border is */
static unsigned int snap                      = 20; /* Snap pixel */
static int nmaster                            = 1;
static int resizehints                        = 0;
static int decorhints                         = 1; /* Respect decoration hints */
static int savefloat                          = 1; /* Save position of floating windows */
static int refreshrules                       = 1; /* Refresh rules when a CLASS or TITLE changes */
static int i3nmaster                          = 0; /* Enable i3-gaps like nmaster (1/0) */
static int mousemfact                         = 1; /* Enable adjusting mfact using the mouse (1/0) */
static float mfact                            = 0.50;

/* Window gap options */
static int enablegaps                         = 1; /* Enable gaps */
static unsigned int gappih                    = 10; /* horiz inner gap between windows */
static unsigned int gappiv                    = 10; /* vert inner gap between windows */
static unsigned int gappoh                    = 10; /* horiz outer gap between windows and screen edge */
static unsigned int gappov                    = 10; /* vert outer gap between windows and screen edge */
static unsigned int smartgapsize              = 0; /* Gaps for smartgaps. If set to zero, gaps will be disabled. */
static int smartgaps                          = 0; /* 1 means use different gap size when there is only one window */

/* Window aesthetic options */
static int fadeinactive                       = 1; /* Fade inactive windows */
static double activeopacity				      = 1.0f; /* Window opacity when it's focused (0 <= opacity <= 1) */
static double inactiveopacity                 = 0.875f; /* Window opacity when it's inactive (0 <= opacity <= 1) */
static Bool bUseOpacity                       = True; /* Starts with opacity on any unfocused windows */

/* Rounded corners
 * In order to use:
 * - borderpx must also be set to 0 or else rounded corners will be disabled.
 */
static int roundedcorners                     = 0; /* Enable (1) rounded corners or disable (0) rounded corners. */
static int cornerradius                       = 3; /* Radius of rounded corners, 10 is the default. */

/* Tag preview options */
static int tagpreview                         = 1; /* Enable tag previews */
static int mousepreview                       = 1; /* Display tag previews if hovering over a tag */
static int scalepreview                       = 4; /* Size of tag preview */

/* Window spawning options */
static int spawncd                            = 1; /* Spawn clients in the working directory of the focused client */
static int attachdirection                    = 3; /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */
static int swallowclients                     = 1; /* Swallow windows or not */
static int swallowfloating                    = 1; /* Swallow floating windows by default */
static int centerfloating                     = 1; /* Center floating windows by default */
static int startontag                         = 1; /* Start on a tag or not? */
static int floatscratchpad                    = 0; /* Float the scratchpad window on hide (1/0) */

/* Font options */
static char font[]                            = { "fontawesome:size=8" }; /* What font should we use? */
static char font2[]                           = { "NotoSans-Regular:size=8:antialiasing=true" }; /* Second font */
static char font3[]                           = { "Noto Emoji:size=8" }; /* Third font */
static const char *fonts[]                    = { font, font2, font3 };
static char defaultname[]                     = ""; /* What to print when a status bar is not running */

/* Bar options */
static char status[]                          = "status"; /* Status bar to use, dwmblocks for dwmblocks, slstatus for slstatus, etc. */

/* Alternate bar
 * 
 * Some users may prefer to use a different bar than what dwm provides.
 * Polybar users for example might want to use their bspwm bar in dwm.
 * If so, follow these instructions:
 * - Use xprop to find the class (WM_CLASS) and add that to altbarclass
 * - Set altbarcmd to the bar you want to use.
 * - Set altbar to 1 (0 will disable it).
 * - Recompile dwm and restart it using 'dwm_run -r'
 * 
 * If you didn't do any changes to the source code, just run dwm_run -r'
 * If you use Polybar, the default settings here will work, just set altbar to 1 and set altbarcmd to 'polybar &'.
 */
static char *altbarcmd		                  = "";
static int altbar                             = 0; /* 1 means use non-dwm status bar */
static char *altbarclass                      = "Polybar"; /* Alternate bar class name */
static char *alttrayname                      = "tray"; /* Polybar tray instance name */

/* Misc */
static char shell[]                           = "/bin/sh"; /* Shell to use */ 
static char *scratchpadcmd[]                  = {"s", TERMINAL, "-t", "scratchpad", NULL};
static int movefullscreenmon                  = 1; /* Move fullscreen windows to another monitor */
static int lockfullscreen                     = 1;
static int warpcursor                         = 1; /* Warp cursor when switching client/monitor */
static int pertag                             = 1; /* Use different mfact and layout for each layout */
static int wmclass                            = 1; /* Enable (1) workaround for when a class cannot be grabbed */
static int clicktofocus                       = 0; /* Click to focus a window, instead of focusing when touched. (1/0) */

/* Icon options */
static int sizeicon                           = 10; /* size of the icon */
static int spacingicon                        = 5; /* spacing between the title and icon */

/* Bar options */
static int barheight                          = 5; /* Bar height in px, 0 = calculate automatically */
static int barposition                        = 1; /* Bar position. Top: 0, Bottom: 1 */
static int barpaddingv                        = 0; /* How much padding to have vertically in pixels */
static int barpaddingh                        = 0; /* How much padding to have horizontally in pixels */
static int leftlayout                         = 1; /* Layout indicator on the left (1) or on the right (0) */

/* Tag text options */
static int underline                          = 0; /* Underline tags (1) or not (0) */
static int underlineall 		              = 0; /* 1 to show underline on all tags, 0 for just the active ones */
static int underlinepad	                      = 5; /* Horizontal padding between the underline and tag */
static int underlinestroke	                  = 2; /* Height of the underline */
static int underlinevoffset	                  = 0; /* How far above the bottom of the bar the line should appear */

/* Bar item options */
static int showbar                            = 1; /* Show the bar or not? */
static int hidelayout                         = 0; /* Hide layout indicator (1) or show (0) */
static int hidetitle                          = 0; /* Hide title (1) or show (0) */
static int hidestatus                         = 0; /* Hide status bar (1) or show (0) */
static int hidetags                           = 0; /* Hide status bar (1) or show (0) */
static int hidefloating                       = 0; /* Hide floating indicator (1) or show (0) */
static int hidesticky                         = 0; /* Hide sticky indicator (1) or show (0) */

/* Status options */
static int statusallmons                      = 1; /* Draw status bar on all monitors */

/* Layout options */
static unsigned int monocleclientcount        = 0; /* Display client count in the Monocle layout */
static unsigned int monoclecount              = 0; /* Display focused client and number of total clients in the Monocle layout */
static char monocleformat[]                   = "[%d/%d]"; /* Format of the monocle count. Unsigned integer monoclecount must be set to 1 for this to be used. */
static unsigned int deckcount                 = 0; /* Display deck count in the deck layout */
static char deckformat[]                      = "D %d"; /* Format of the deck count. deckcount must be set to 1 for this to be used. */
static unsigned int stairpx                   = 20; /* depth of the stairs layout */
static int stairdirection                     = 1; /* 0: left-aligned, 1: right-aligned */
static int stairsamesize                      = 1; /* 1 means shrink all the staired windows to the same size */
static int defaultlayout                      = 1; /* Reset layout when there is only one client visible */
static int forcevsplit                        = 1; /* Force two clients to always split vertically in nrowgrid layout */

/* Bar colors */
static char col_background[]                  = "#222222"; /* dwm dark bg & slstatus bg */
static char col_backgroundmid[]               = "#222222"; /* dwm middle background */

/* General text colors */
static char col_textnorm[]                    = "#bbbbbb"; /* application title bar/font for norm */
static char col_textsel[]                     = "#eeeeee"; /* dwm text/font for selected */

/* Window border colors */
static char col_windowbordernorm[]            = "#000000"; /* dwm norm window border */
static char col_windowbordersel[]             = "#eeeeee"; /* dwm sel window border */

/* Tag text/background colors */
static char col_tag1[]                        = "#333333"; /* tag 1 background */
static char col_tag1_text[]                   = "#eeeeee"; /* tag 1 text (fg) */
static char col_tag2[]                        = "#333333"; /* tag 2 background */
static char col_tag2_text[]                   = "#eeeeee"; /* tag 2 text (fg) */
static char col_tag3[]                        = "#333333"; /* tag 3 background */
static char col_tag3_text[]                   = "#eeeeee"; /* tag 3 text (fg) */
static char col_tag4[]                        = "#333333"; /* tag 4 background */
static char col_tag4_text[]                   = "#eeeeee"; /* tag 4 text (fg) */
static char col_tag5[]                        = "#333333"; /* tag 5 background */
static char col_tag5_text[]                   = "#eeeeee"; /* tag 5 text (fg) */
static char col_tag6[]                        = "#333333"; /* tag 6 background */
static char col_tag6_text[]                   = "#eeeeee"; /* tag 6 text (fg) */
static char col_tag7[]                        = "#333333"; /* tag 7 background */
static char col_tag7_text[]                   = "#eeeeee"; /* tag 7 text (fg) */
static char col_tag8[]                        = "#333333"; /* tag 8 background */
static char col_tag8_text[]                   = "#eeeeee"; /* tag 8 text (fg) */
static char col_tag9[]                        = "#333333"; /* tag 9 background */
static char col_tag9_text[]                   = "#eeeeee"; /* tag 9 text (fg) */

/* Layout indicator colors */
static char col_layouttext[]                  = "#000000"; /* Layout indicator text (fg) */
static char col_layoutbgnorm[]                = "#222222"; /* Layout indicator background (norm) */
static char col_layoutbgsel[]                 = "#bbbbbb"; /* Layout indicator background (norm) */

/* status2d colors */
static char col_status0[]                     = "#131210";
static char col_status1[]                     = "#bf616a";
static char col_status2[]                     = "#A16F9D";
static char col_status3[]                     = "#68ABAA";
static char col_status4[]                     = "#A89F93";
static char col_status5[]                     = "#D3A99B";
static char col_status6[]                     = "#AFC9AC";
static char col_status7[]                     = "#eae1cb";
static char col_status8[]                     = "#a39d8e";
static char col_status9[]                     = "#6D5E8E";
static char col_status10[]                    = "#A16F9D";
static char col_status11[]                    = "#D3A99B";
static char col_status12[]                    = "#AFC9AC";
static char col_status13[]                    = "#eae1cb";
static char col_status14[]                    = "#6D5E8E";
static char col_status15[]                    = "#ffffff";

/* Opacity settings
 * These options set the opacity of the status bar modules.
 * They can be anything from 0 (fully transparent) to 255 (fully opaque).
 *
 * You can also set OPAQUE and TRANSPARENT which are the same as 0 and 255 respectively.
 */
static const unsigned int baropacity          = 160; /* Opacity for the overall bar */
static const unsigned int layoutopacity       = 160; /* Opacity for the layout indicator */
static const unsigned int tagnormopacity      = OPAQUE; /* Opacity for other tags */
static const unsigned int tagselopacity       = OPAQUE; /* Opacity for the selected tag */
static const unsigned int normtitleopacity    = 160; /* Opacity for all other windows in the dwm bar */
static const unsigned int seltitleopacity     = 160; /* Opacity for the focused window in the dwm bar */
static const unsigned int hiddenopacity       = 0; /* Opacity for hidden/minimized windows */
static const unsigned int statusopacity       = 180; /* Opacity for dwm status bar */

/* Tag text options */
static char *tags[]                           = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
/* NOTE: alttags will not be used if taglabels are used. */
static char *alttags[]                        = { "[1]", "[2]", "[3]", "[4]", "[5]", "[6]", "[7]", "[8]", "[9]" };

/* Icon misc */
static XPoint stickyicon[]                    = { {0,0}, {4,0}, {4,8}, {2,6}, {0,8}, {0,0} }; /* represents the icon as an array of vertices */
static XPoint stickyiconbb                    = {4,8};	/* defines the bottom right corner of the polygon's bounding box (speeds up scaling) */
