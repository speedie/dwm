/* These are all available layouts in this build of dwm.
 * Any new layouts should be added in the exact same way as existing layouts.
 * Layouts that are added through patches should be in layouts.c
 *
 * Once you're done with your edits, run 'make clean install'. */

static const Layout layouts[]                 = {
    { "(L1)",          tile },
	{ "(L2)",          NULL },
	{ "(L3)",          monocle },
	{ "(L4)",          grid },
    { "(L5)",          deck },
    { "(L6)",          centeredmaster },
	{ "(L7)",          centeredfloatingmaster },
	{ "(L8)",          spiral },
	{ "(L9)",          dwindle },
	{ "(L10)",         tcl },
	{ "(L11)",         bstack },
	{ "(L12)",         bstackhoriz },
	{ "(L13)",         horizgrid },
	{ "(L14)",         tatami },
	{ "(L15)",	       tilewide },
	{ "(L16)",         stairs },
	{ "(L17)",         tile54 },
	{ "(L18)",         col },
	{ "(L19)",         dynamicgrid },
	{ NULL,            NULL },
};
