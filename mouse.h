/* Mouse bindings
 *
 * This header contains mouse binds.
 * Change them, or remove them if you prefer. You can also add more if you want.
 * Once you're done with your edits, run 'make clean install'.
 */

/* Actions when the mouse clicks a part of the screen */
static Button buttons[] = {
	/* click         event mask button   function        argument */
	{ ClkLtSymbol,   0,         Button3, layoutmenu,     {0} },
	{ ClkLtSymbol,   0,         Button1, cyclelayout,    {.i = +1 } },
	{ ClkLtSymbol,   0,         Button2, cyclelayout,    {.i = -1 } },
	{ ClkWinTitle,   0,         Button2, zoom,           {0} },
	{ ClkClientWin,  MODKEY,    Button1, moveorplace,    {.i = 1} },
	{ ClkClientWin,  MODKEY,    Button2, togglefloating, {0} },
	{ ClkClientWin,  MODKEY,    Button3, resizemouse,    {0} },
	{ ClkStatusText, 0,         Button1, spawn,          {.v = statuscmd } },
	{ ClkStatusText, 0,         Button2, spawn,          {.v = statuscmd } },
	{ ClkStatusText, 0,         Button3, spawn,          {.v = statuscmd } },
	{ ClkWinTitle,   0,         Button1, togglewin,      {0} },
	{ ClkWinTitle,   0,         Button3, spawn,          SHCMD("dwm-utils") },
	{ ClkTagBar,     0,         Button1, view,           {0} },
};

/* Commands that will run when a part of the status bar is pressed
 * Built in status bar does not have support for this yet but you can add it by adding:
 * '\x<num>
 * before you print anything. Keep in mind <num> MUST be two digits.
 * 
 * Invalid: xsetroot -name "\x01This is a test"
 * Valid: xsetroot -name "$(printf '\x01This is a test')"
 */
static const StatusCmd statuscmds[] = {
	{ "notify-send 'Status item (1) was pressed.'",     1 },
	{ "notify-send 'Status item (2) was pressed.'",     2 },
	{ "notify-send 'Status item (3) was pressed.'",     3 },
};
